<?php
/* Change defautl get terms in wp */
add_filter('get_terms','sorting_priority_list_categories',10,2);
if(!function_exists('sorting_priority_list_categories')):
  function sorting_priority_list_categories($terms,$taxonomy){
    if(in_array('category',$taxonomy) && is_singular('post')){
        $sorting_terms = array();
        $temp_terms = array();
        foreach ($terms as $key => $term) {
          $sorting_priority = get_term_meta( $term->term_id, 'cat-sorting-priority', true );
          if($sorting_priority){
            $sorting_terms[] = $term;
          }else{
            $temp_terms[] = $term;
          }
        }
        $terms = array_merge($sorting_terms,$temp_terms);
    }
    return $terms;
  }
endif;

?>
