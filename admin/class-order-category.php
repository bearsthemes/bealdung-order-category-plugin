<?php
/**
 * Created by TomPham
 * Date: 07/11/2019
 */

class OptionSortingPriority{

	function __construct() {
    /* Edit Category Label */
    add_action( 'category_edit_form_fields', array( $this, 'edit_sorting_priority_filed' ) );
    /* Update Category Label */
    add_action( 'edited_category', array( $this, 'update_sorting_priority_field' ) );

	}

	function update_sorting_priority_field( $term_id ) {
		if ( isset( $_POST['cat-sorting-priority'] ) && '' !== $_POST['cat-sorting-priority'] ) {
			update_term_meta( $term_id, 'cat-sorting-priority', $_POST['cat-sorting-priority'] );
		}else{
      delete_term_meta( $term_id, 'cat-sorting-priority' );
    }
	}

	function edit_sorting_priority_filed( $term ) {
		// get sorting priority value
		$sorting_priority = get_term_meta( $term->term_id, 'cat-sorting-priority', true );
		?>
        <tr class="form-field term-group-wrap">
            <th scope="row">
              <label for="cat-sorting-priority"><?php _e( "Sorting Priority", 'sorting-priority-category-labels' ); ?></label>
            </th>
            <td>
              <input type="checkbox" <?php echo checked( $sorting_priority, 'on', false )?> name="cat-sorting-priority" id="cat-sorting-priority-field"/>
              <p class="description"> <?php echo __('it should appear as the leftmost category in the list','sorting-priority-category-labels') ?> </p>
            </td>
        </tr>
    <?php
	}
}

/* only run in admin */
if ( is_admin() ) {
	$option_sorting_priority = new OptionSortingPriority();
}
