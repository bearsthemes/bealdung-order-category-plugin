=== Sorting Priority Category Labels ===

This plugins allows you to display checked "Sorting Priority" in categories.
When a category has sorting priority it should appear as the leftmost category in the list.
In the case when two or more categories that have sorting priority are displayed, the resulting order of these categories should be alphabetical.

Ordered list:

1. Sorting Priority for Category Labels

== Installation ==
1. Upload "sorting-priority-category-labels.php" to the "/wp-content/plugins/" directory.
2. Activate the plugin through the "Plugins" menu in WordPress.
3. Go menu "Categories" in Posts

= 1.0 =
* Initial release.
