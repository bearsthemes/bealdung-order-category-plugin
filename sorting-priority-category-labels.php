<?php
/*
Plugin Name: Sorting Priority Category Labels
Plugin URI: https://www.baeldung.com/
Description: This plugins allows you to display checked "Sorting Priority" in categories. When a category has sorting priority it should appear as the leftmost category in the list. In the case when two or more categories that have sorting priority are displayed, the resulting order of these categories should be alphabetical.
Author: Tom Pham
Text Domain: sorting-priority-category-labels
Domain Path: /languages
Version: 1.0
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define( 'SORTING_PRIORITY_URL', plugin_dir_url( __FILE__ ) );

/* Setting Backend */
require_once('admin/class-order-category.php');

/* Front End */
require_once('front-end/order-list-categories.php');
